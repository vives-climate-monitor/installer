const shell = require('shelljs')
const fs = require('fs')
const path = require('path')
const colors = require('colors');

Log = {}
module.exports = Log;

Log.types = {
    DEBUG: "debug",
    INFO: "info",
    ERROR: "error",
    WARNING: "warning",
    DATA: "data"
}

Log.names = {
    SENSOR: "sensor",
    INSTALLER: "installer"
}

// set theme
colors.setTheme({
    info: 'green',
    data: 'grey',
    warning: 'yellow',
    debug: 'blue',
    error: 'red',
    message: 'white',
});

Log.name = Log.names.INSTALLER;
Log.logFile = `/var/log/climate-monitor/${Log.name}.log`;

Log.log = function (type, message) {
    // console log message
    switch (type) {
        case Log.types.INFO:
            console.log(colors.data("[ " + (new Date().toUTCString()) + " ] [ ") + colors.info(type) + colors.data(" ] [ " + Log.name + " ] ") + colors.message(message))
            break;
        case Log.types.ERROR:
            console.log(colors.data("[ " + (new Date().toUTCString()) + " ] [ ") + colors.error(type) + colors.data(" ] [ " + Log.name + " ] ") + colors.message(message))
            break;
        case Log.types.WARNING:
            console.log(colors.data("[ " + (new Date().toUTCString()) + " ] [ ") + colors.warning(type) + colors.data(" ] [ " + Log.name + " ] ") + colors.message(message))
            break;
        case Log.types.DEBUG:
            console.log(colors.data("[ " + (new Date().toUTCString()) + " ] [ ") + colors.debug(type) + colors.data(" ] [ " + Log.name + " ] ") + colors.message(message))
            break;
        default:
            if (typeof message == "undefined")    // only one paramter given => message is in 'type' variable
                console.log(colors.data("[ " + (new Date().toUTCString()) + " ] [ ") + colors.debug("debug") + colors.data(" ] [ " + Log.name + " ] ") + colors.message(type))
            else    // unknown type given
                console.log(colors.data("[ " + (new Date().toUTCString()) + " ] [ ") + colors.debug("debug") + colors.data(" ] [ " + Log.name + " ] ") + colors.message(message))
            break;
    }
    // check if /var/log/climate-monitor exists
    if(!fs.existsSync("/var/log/climate-monitor")) {
        fs.mkdirSync("/var/log/climate-monitor")
        Log.log(Log.types.INFO, `make climate-monitor folder in /var/log`)
    }
    // save message to file
    if (typeof message == "undefined")    // only one paramter given => message is in 'type' variable
        var text = "[ " + (new Date().toUTCString()) + " ] [ " + "debug" + " ] [ " + Log.name + " ] " + type + ";;\r\n\r\n"
    else if (typeof type == "undefined")   // unknown type given
        var text = "[ " + (new Date().toUTCString()) + " ] [ " + "debug" + " ] [ " + Log.name + " ] " + message + ";;\r\n\r\n"
    else
        var text = "[ " + (new Date().toUTCString()) + " ] [ " + type + " ] [ " + Log.name + " ] " + message + ";;\r\n\r\n"
    fs.appendFile(Log.logFile, text, (err) => {
        if (err) {
            console.log(colors.data("[ " + (new Date().toUTCString()) + " ] [ ") + colors.error("error") + colors.data(" ] [ " + Log.name + " ] ") + colors.message("cannot save to log file"))
        };
    });
}