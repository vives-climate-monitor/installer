const gpio = require('onoff').Gpio;
const Config = require('./config');
const fs = require('fs');
const shell = require('shelljs')
const path = require('path')
const Log = require('./consoleLog');



class Reset {
    constructor() {
        Log.log(Log.types.INFO, 'Reset module initialisated')
        this.led = new gpio(24, 'out');
        this.button = new gpio(18, 'in', 'both', { debounceTimeout: 50 });
        this.pressTimeInSec = 5;
        this.ledOn = false;

        this.resetTimout;

        this.button.watch((err, value) => {
            if (value == 0) {
                this.resetTimout = setTimeout(() => {
                    this.resetModule();
                }, this.pressTimeInSec * 1000)
            } else {
                clearTimeout(this.resetTimout);
            }
        })
    }

    async resetModule() {
        // start error led
        var ledInterval = setInterval(() => {
            this.led.writeSync(this.ledOn)
            this.ledOn = !this.ledOn;
        }, 200)
        const config = await Config.getInstance();
        // delete logs
        if (fs.existsSync('/var/log/climate-monitor/')) {
            files = fs.readdirSync('/var/log/climate-monitor/');
            files.forEach((file) => {
                var file_path = `/var/log/climate-monitor/${file}`;
                if (fs.existsSync(file_path)) {
                    try {
                        fs.unlinkSync(file_path);
                    } catch (error) {
                        Log.log(Log.types.ERROR, `failed to remove ${file_path}: ${error}`);
                    }
                }
            })
        }
        Log.log(Log.types.ERROR, "reset module")

        // delete metingen
        if (fs.existsSync('/home/mi/installer/data/metingen/')) {
            var files = fs.readdirSync('/home/pi/installer/data/metingen/');
            files.forEach((file) => {
                var file_path = `/home/pi/installer/data/metingen/${file}`;
                if (fs.existsSync(file_path)) {
                    try {
                        fs.unlinkSync(file_path);
                    } catch (error) {
                        Log.log(Log.types.ERROR, `failed to remove ${file_path}: ${error}`);
                    }
                }
            })
        }

        // reset SSID of access-point 
        var regex = `^ssid=.*$`
        try {
            data = fs.readFileSync("/etc/hostapd/hostapd.conf", 'utf8');
            var result = data.replace(new RegExp(regex, "gm"), "ssid=climate-monitor");
            fs.writeFileSync("/etc/hostapd/hostapd.conf", result, 'utf8');
        } catch (error) {
            Log.log(Log.types.ERROR, `failed to reset /etc/hostapd/hostapd.conf: ${error}`);
        }


        // replace /etc/wpa_supplicant/wpa_supplicant.conf file with /data/reset.wpa_supplicant.conf
        if (fs.existsSync('/etc/wpa_supplicant/wpa_supplicant.conf')) {
            try {
                fs.unlinkSync("/etc/wpa_supplicant/wpa_supplicant.conf");
            } catch (error) {
                Log.log(Log.types.ERROR, `failed to remove /etc/wpa_supplicant/wpa_supplicant.conf: ${error}`);
            }
        }
        if (!fs.existsSync(path.join(__dirname, '../data/reset.wpa_supplicant.conf'))) { // check if  /data/reset.wpa_supplicant.conf exists
            Log.log(Log.types.ERROR, "failed to find template file /data/reset.wpa_supplicant.conf")
        } else {
            fs.copyFileSync(path.join(__dirname, '../data/reset.wpa_supplicant.conf'), '/etc/wpa_supplicant/wpa_supplicant.conf');
        }

        // Reset hostname in /etc/hosts
        regex = `^127\.0\.1\.1\t*${config.hostname}|^127\.0\.1\.1 *${config.hostname}`
        try {
            var data = fs.readFileSync("/etc/hosts", 'utf8');
            var result = data.replace(new RegExp(regex, "gm"), "127.0.1.1        climate-monitor");
            fs.writeFileSync("/etc/hosts", result, 'utf8');
        } catch (error) {
            Log.log(Log.types.ERROR, `failed to reset /etc/hosts: ${error}`);
        }
        // Reset hostname in /etc/hostname
        regex = `^${config.hostname}$`
        try {
            data = fs.readFileSync("/etc/hostname", 'utf8');
            var result = data.replace(new RegExp(regex, "gm"), "climate-monitor");
            fs.writeFileSync("/etc/hostname", result, 'utf8');
        } catch (error) {
            Log.log(Log.types.ERROR, `failed to reset /etc/hostname: ${error}`);
        }

        // remove sensor container
        Log.log(Log.types.INFO, `start removing containers`)
        var cmd_rm = await shell.exec('docker stop sensor ouroboros && docker rm sensor ouroboros', { silent: false });
        Log.log(Log.types.INFO, `containers removed`)

        // run docker-compose.yml file
        Log.log(Log.types.INFO, `recreate containers`)
        var cmd_run = await shell.exec('cd /home/pi/installer && docker-compose up -d', { silent: false });
        Log.log(Log.types.INFO, `containers recreated`)

        // delete data/config.json file
        if (fs.existsSync(path.join(__dirname, '../data/config.json'))) {
            try {
                fs.unlinkSync(path.join(__dirname, '../data/config.json'));
            } catch (error) {
                Log.log(Log.types.ERROR, `failed to remove data/config.json: ${error.message}`);
            }
        }

        // restart module
        shell.exec('reboot');
    }

    static async getInstance() {
        if (!Reset.instance) {
            Reset.instance = await new Reset()
        }
        return Reset.instance
    }
}

module.exports = Reset
