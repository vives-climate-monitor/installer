const shell = require('shelljs')
const fs = require('fs')
const path = require('path')
const bcrypt = require('bcrypt')
const Log = require('./consoleLog');
var ip = require('ip')
var QRCode = require('qrcode')
var piWifi = require('pi-wifi');
var localIpV4Address = require("local-ipv4-address");

class Config {
    constructor() {
        this.initialVariables = {
            mac: this.getMAC(),
            hostname: this.getHostname(),
            co2Offset: 0,
            tvocOffset: 0,
            tempOffset: 0,
            humOffset: 0,
            ledsInDarkOn: 0,
            ledColorDependingOn: "co2",
            ledsGradiant: false,
            changeBrightnessManually: false,
            manualBrightness: 10,
            fullBrightness: false,
            maxLight: 500,
            colorGradiants: {
                co2: { min: "#00ff00", max: "#ff0000" },
                tvoc: { min: "#00ff00", max: "#ff0000" },
                temp: { min: "#0000ff", max: "#ff0000" },
                hum: { min: "#ff0000", max: "#0000ff" },
                other: { min: "#ff00ff", max: "#ff0000" }
            },
            exportDatabaseSettings: {},
            exportWebhookSettings: {},
            files: [],
            username: 'admin',
            passwordHash: bcrypt.hashSync('admin123', 10)
        } 

        this.getIP();

        if (!fs.existsSync(path.join(__dirname, '../data/config.json'))) {
            Log.log(Log.types.WARNING, `no config yet!`)
            this.initialSetup()
        }

        Log.log(Log.types.INFO, `initialising from data/config.json`)
        this.setupFromJSonFile();

        this.getDownloadFiles();
    }

    setupFromJSonFile() {
        var data = fs.readFileSync(path.join(__dirname, '../data/config.json'))
        // check for new variables sinds last update, and saves these to the file
        data = JSON.parse(data);
        for (var initVar in this.initialVariables) {
            if (typeof data[initVar] == "undefined") {
                data[initVar] = this.initialVariables[initVar]
            }
        }
        Object.assign(this, data)
        this.save();
    }

    async initialSetup() {
        if (!fs.existsSync(path.join(__dirname, '../data'))) {
            fs.mkdirSync(path.join(__dirname, '../data'))
        }
        Object.assign(this, this.initialVariables)
        this.save()
    }

    getMAC() {
        if (shell.exec('which ifconfig', { silent: true }).code !== 0) {
            // net-tools not installed
            shell.exec('apt install net-tools -y', { silent: true })
        }

        return shell.exec('ifconfig wlan0 | grep ether | awk \'{print $2}\' ', { silent: true })
    }

    getHostname() {
        return shell.exec('hostname', { silent: true }).stdout.replace("\n", "");
    }

    save() {
        fs.writeFileSync(path.join(__dirname, '../data/config.json'), JSON.stringify(this))
    }

    update(data) {
        for (var element in data) {
            // update offsets
            if (["co2Offset", "tvocOffset", "tempOffset", "humOffset"].includes(element)) {
                if (data[element] == "None" || data[element] == "none") {
                    this[element] = 0;
                } else {
                    var value = parseFloat(data[element])
                    if (!isNaN(value)) {         // check if value is a number
                        this[element] = value
                    }
                }
            }
            if(element == "hostname" && this.hostname != data[element]) {

                var regex = /^(([a-zA-Z0-9])+-*([a-zA-Z0-9])+)*$/;
                if(regex.test(data[element]))
                {
                    this.updateHostname(data[element])
                }
                else{
                    this.updateHostname(this.hostname)
                }
               
            }
            else if (element == "ledsInDarkOn") {
                this.ledsInDarkOn = data[element];
            }
            else if (element == "ledColorDependingOn") {
                this.ledColorDependingOn = data[element];
            }
            else if (element == "ledsGradiant") {
                this.ledsGradiant = data[element];
            }
            else if (element == "colorGradiants") {
                this.colorGradiants = data[element];
            }
            else if (element == "manualBrightness") {
                this.manualBrightness = data[element];
            }
            else if (element == "changeBrightnessManually") {
                this.changeBrightnessManually = data[element];
            }
            else if (element == "maxLight") {
                this.maxLight = data[element];
            }
            else if (element == "fullBrightness") {
                this.fullBrightness = data[element];
            }
        }
        this.save()
    }

    updateDBSettings(data) {
        for (var element in data) {
            this['exportDatabaseSettings'][element] = data[element]
        }
        this.save()
    }

    updateWebhookSettings(data) {
        for (var element in data) {
            this['exportWebhookSettings'][element] = data[element]
        }
        this.save()
    }

    scanHostname() {
        this.hostname = this.getHostname();
    }

    updateHostname(newHostname) {
        var oldHostname = this.hostname;
        // Update hostname in /etc/hosts
        regex = `^127\.0\.1\.1\t*${oldHostname}|^127\.0\.1\.1 *${oldHostname}`
        try {
            var data = fs.readFileSync("/etc/hosts", 'utf8');
            var result = data.replace(new RegExp(regex, "gm"), `127.0.1.1       ${newHostname}`);
            fs.writeFileSync("/etc/hosts", result, 'utf8');
        } catch (error) {
            Log.log(Log.types.ERROR, `failed to reset /etc/hosts: ${error}`);
        }
        // Update hostname in /etc/hostname
        regex = `^${oldHostname}$`
        try {
            data = fs.readFileSync("/etc/hostname", 'utf8');
            var result = data.replace(new RegExp(regex, "gm"),newHostname);
            fs.writeFileSync("/etc/hostname", result, 'utf8');
        } catch (error) {
            Log.log(Log.types.ERROR, `failed to reset /etc/hostname: ${error}`);
        }
        // Update hostname in /etc/hostapd/hostapd.conf
        var regex = `^ssid=.*$`
        try {
            data = fs.readFileSync("/etc/hostapd/hostapd.conf", 'utf8');
            var result = data.replace(new RegExp(regex, "gm"), `ssid=${newHostname}`);
            fs.writeFileSync("/etc/hostapd/hostapd.conf", result, 'utf8');
        } catch (error) {
            Log.log(Log.types.ERROR, `failed to reset /etc/hostapd/hostapd.conf: ${error}`);
        }
        this.hostname = newHostname;
        this.save()
    }

    getPasswordHash() {
        return this.passwordHash;
    }

    static async getInstance() {
        if (!Config.instance) {
            Config.instance = await new Config()
        }
        return Config.instance
    }

    getIP() {
        // get IP of hostname
        var networks = require('os').networkInterfaces()

        if(networks.wlan0){
            var ipv4 = networks.wlan0[0].address
        }
        else if(networks.eth0){
            var ipv4 = networks.eth0[0].address
        }
        this.ip = `http://${ipv4}`;
        //this.ip = `http://${ip.address()}`;

        QRCode.toFile('/home/pi/installer/public/images/QRcode.png', this.ip, {
            color: {
                dark: '#000',  // Red dots #E31D1A
                light: '#0000' // Transparent background
            }
        }, function (err) {
            if (err) Log.log(Log.types.ERROR, `Failed to save QR-code to file: ${err.message}`)
        })
    }

    getDownloadFiles() {
        var AllFiles = [];
        var PM2logFiles = [
            "/var/log/climate-monitor/",    // log files of consoleLog class => these are used to show on website
            "/home/pi/.pm2/logs/",          // log files of scirpt "watch:package.json"     => errors that we didn't catch can you see here (stderr en stdout)
            "/root/.pm2/logs/",             // log files of app.js script                   => errors that we didn't catch can you see here (stderr en stdout)
            "/root/.pm2/pm2.log",           // log file of backend js files watcher
            "/home/pi/.pm2/pm2.log"         // log file of package.json watcher
        ]
        var dataFiles = [
            "/home/pi/installer/data/metingen/"
        ]
        var DockerFiles = [
            "/var/lib/docker/containers"    // all info from the containers are stored here
        ]
        // pm2 log files
        PM2logFiles.forEach((f) => {
            if (fs.existsSync(f)) {
                var stats = fs.statSync(f);
                if (stats.isFile()) {
                    AllFiles.push({
                        path: f,
                        name: f.split('/')[f.split('/').length - 1]
                    });
                } else if (stats.isDirectory()) {
                    var files = fs.readdirSync(f);
                    files.forEach((file) => AllFiles.push({
                        path: f + file,
                        name: file
                    }));
                }
            }
        })
        // metingen log files
        dataFiles.forEach((f) => {
            if (fs.existsSync(f)) {
                var files = fs.readdirSync(f);
                files.forEach((file) => AllFiles.push({
                    path: f + file,
                    name: file
                }));
            }
        })
        // docker log files
        var sensorContainerID = shell.exec('docker ps -aqf "name=sensor"', { silent: true }).stdout.trimEnd()
        var ouroborosContainerID = shell.exec('docker ps -aqf "name=ouroboros"', { silent: true }).stdout.trimEnd()
        DockerFiles.forEach((f) => {
            if(fs.existsSync(f)) {
                var folders = fs.readdirSync(f);
                folders.forEach(folder => {
                    if(folder.indexOf(sensorContainerID) == 0) {
                        var path = f + '/' + folder + '/' + folder + '-json.log'
                        if(fs.existsSync(path)) {
                            AllFiles.push({
                                path: path,
                                name: "docker-sensor.log"
                            });
                        }
                    }
                    else if(folder.indexOf(ouroborosContainerID) == 0) {
                        var path = f + '/' + folder + '/' + folder + '-json.log'
                        if(fs.existsSync(path)) {
                            AllFiles.push({
                                path: path,
                                name: "docker-ouroboros.log"
                            });
                        }
                    }
                })
            }
        })
        this.files = AllFiles;
        this.save();
    }
}

module.exports = Config