const shell = require('shelljs')
const fs = require('fs')
const path = require('path')
const Log = require('./consoleLog');

class Wifi {
    constructor() {
        this.listOfKnowingSSIDs = []
        this.wpaFilePath = '/etc/wpa_supplicant/wpa_supplicant.conf'

        Log.log(Log.types.INFO, `initialising from /etc/wpa_supplicant/wpa_supplicant.conf`)

        this.listOfNetworksOfWpaFile = this.getAllNetworksOfWpafile()
        this.listOfBroadcastedNetworks = this.getAllBroadcastedNetworks()
        this.connectedNetwork = this.getConnectedNetwork()
    }

    connectToNetwork(networkCredentials) {
        let data = fs.readFileSync(this.wpaFilePath, 'utf8')
        // check if network is already in wpa file => delete if exists
        if (this.removeNetworkFromWpaFile(networkCredentials.SSID)) {
            data = fs.readFileSync(this.wpaFilePath, 'utf8') // read file again to make update the string
        }
        // add network in wpa file
        let textToAdd = `\nnetwork={\n\tssid="${networkCredentials.SSID}"`
        if (networkCredentials.noPSK) {
            // login without password
            textToAdd += '\n\tkey_mgmt=NONE'
        } else {
            // login with password
            textToAdd += `\n\tkey_mgmt=WPA-PSK\n\tpsk="${networkCredentials.PSK}"`
        }
        textToAdd += '\n}\n'
        const newData = data + textToAdd
        fs.writeFileSync(this.wpaFilePath, newData)
        // try connect to wifi network
        shell.exec('autohotspot', {silent:true})
        // update network list
        this.listOfNetworksOfWpaFile = this.getAllNetworksOfWpafile()
    }

    removeNetworkFromWpaFile(ssid) {
        const data = fs.readFileSync(this.wpaFilePath, 'utf8')
        if (data.includes(`ssid="${ssid}"`)) {
            // search for 'network={' closest to the position of the ssid
            const ssidPos = data.indexOf(`ssid="${ssid}"`)
            let startNetworkPos = 0
            let stopWhile = false
            while (!stopWhile) {
                const pos = data.indexOf('network={', startNetworkPos + 1)
                if (pos < ssidPos && pos != -1) {
                    startNetworkPos = pos
                } else {
                    stopWhile = true
                }
            }
            // remove everything between 'network={' and '}'
            const endNetworkPos = data.indexOf('}\n', ssidPos) + 2
            const substring = data.substring(startNetworkPos - 1, endNetworkPos)
            const newDataList = data.split(substring)
            const newData = newDataList[0] + newDataList[1]
            fs.writeFileSync(this.wpaFilePath, newData)

            // update network list
            this.listOfNetworksOfWpaFile = this.getAllNetworksOfWpafile()
            return true
        }
        // update network list
        this.listOfNetworksOfWpaFile = this.getAllNetworksOfWpafile()
        return false
    }

    getAllNetworksOfWpafile() {
        if(fs.existsSync(this.wpaFilePath)) {
            const data = fs.readFileSync(this.wpaFilePath, 'utf8')
            let newStart = 0
            const listOfNetworks = new Array()
            const iterations = data.split('network={').length - 1
            for (let count = 0; count < iterations; count++) {
                const start = data.indexOf('network={', newStart)
                const network = {}
                const ssidStart = data.indexOf('ssid="', start) + 6
                const ssidStop = data.indexOf('"\n', ssidStart)
                network.SSID = data.slice(ssidStart, ssidStop)
    
                const key_mgmtStart = data.indexOf('key_mgmt=', start) + 9
                const key_mgmtStop = data.indexOf('\n', key_mgmtStart)
                network.key_mgmt = data.slice(key_mgmtStart, key_mgmtStop)
                listOfNetworks.push(network)
                newStart = key_mgmtStop
            }
            return (listOfNetworks)
        }
    }

    getAllBroadcastedNetworks() {
        const outputCMD = shell.exec('autohotspot | grep SSID: ', {silent:true})
        const listOfNetworks = outputCMD.split('\n')
        const newListOfNetworks = []
        listOfNetworks.forEach(element => {
            const startP = element.indexOf('SSID: ') + 6
            const ssid = element.slice(startP)
            if (ssid.length > 1 && !newListOfNetworks.includes(ssid)) { newListOfNetworks.push(ssid) }
        })
        return newListOfNetworks
    }

    getConnectedNetwork() {
        const outputCMD = shell.exec('iwgetid | grep wlan0', {silent:true})
        const startP = outputCMD.indexOf('ESSID:"') + 7
        const ssid = outputCMD.slice(startP, -2)
        return ssid
    }

    static async getInstance() {
        if (!Wifi.instance) {
            Wifi.instance = await new Wifi()
        }
        return Wifi.instance
    }
}

module.exports = Wifi
