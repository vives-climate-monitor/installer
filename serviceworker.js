const CURRENT_CACHE = 'version1'

self.addEventListener('install', event => {
    console.log('service worker installed ');
    if (!'caches' in self) return;
    event.waitUntil(
        caches.open(CURRENT_CACHE)
            .then(cache => {
                //vraagt data van server geeft promise undefined terug als klaar
                return cache.addAll([
                    '../views/offline.ejs'
                ]);
            })

    );
});

self.addEventListener('activate', event => {

    event.waitUntil(caches.keys().then(cacheKeys =>
        {
            console.log(cacheKeys);
            return Promise.all(
                cacheKeys.map(cacheKey => {
                    if(cacheKey !== CURRENT_CACHE)
                    {
                        console.log('deleting cache ' + cacheKey);
                        return caches.delete(cacheKey);
                        
                    }
                })
            )
        })
        )
});

// zorg dat alle bestanden automatisch gecached worden bij eerste aan vraag
self.addEventListener('fetch', event => {
    if (!navigator.onLine && event.request.url.indexof('index.html') !== -1)
        event.respondWith(showOfflineLanding(event));
    else
        event.respondWith(pullFromCache(event));
});

function showOfflineLanding(event) {
    return caches.match(new Request('offline.html'))
}
function pullFromCache(event) {
    return caches.match(event.request)
        .then(response => {
            return response || fetch(event.request)
                .then(response => {
                    console.log('fetched from the network');
                    return caches.open('version7')
                        .then(cache => {
                            cache.put(event.request, response.clone())
                            return response;
                        });
                });
        })
}