const express = require('express')
const path = require('path')
var http = require('http')
var axios = require('axios');
const shell = require('shelljs')
var session = require('express-session')
const bcrypt = require('bcrypt');
const fs = require('fs')
const saltRounds = 10;
var QRCode = require('qrcode');

const router = express.Router()
var sess;

const Config = require('../classes/config');
const Wifi = require('../classes/wifi');
const Gpio = require('../classes/reset.js');
const ConsoleLog = require('../classes/consoleLog');
const Log = require('../classes/consoleLog');

/* GET home page. */
router.get('/', async (req, res) => {
    sess = req.session;
    const config = await Config.getInstance()
    const wifi = await Wifi.getInstance()
    res.render('measurements', { config, sess })
})

router.get('/settings', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        const wifi = await Wifi.getInstance()
        res.render('index', { config, wifi, sess })
    }
    else {
        res.redirect('/login');
    }
})

router.get('/logs', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        res.render('logs', { config, sess })
    }
    else {
        res.redirect('/login');
    }
})

router.get('/login', async (req, res) => {
    sess = req.session;
    const config = await Config.getInstance()
    res.render('login', { config, sess })
})

router.get('/downloads', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        res.render('downloads', { config, sess })
    }
    else {
        res.redirect('/login');
    }
})

// we reroute the get request because if the hostname changes, it's more difficult to send a get request to the sensor container.
// with this way, the backend can send a get request to the localhost and the frontend sends to its own webserver
router.get('/getData/:range', async (req, res) => {
    axios.get(`http://localhost:3000/getData/${req.params.range}`)
        .then((response) => {
            res.send(response.data)
        })
})

router.get('/measurements', async (req, res) => {
    res.redirect('/');
})

router.get('/logout', (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return Log.log(Log.types.ERROR, `failed to destroy admin session ${err}`);
        }
        res.redirect('/');
    });
});

/* POST methods */
router.post('/save', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        config.update(req.body)
        res.json(config)
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/saveAdvancedSettings', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        config.update(req.body)
        res.json(config)
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/saveExportDB', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        config.updateDBSettings(req.body.exportDB)
        res.json(config)
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/saveExportWebhook', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        config.updateWebhookSettings(req.body.exportWebhook)
        res.json(config)
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/connect', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const wifi = await Wifi.getInstance()
        wifi.connectToNetwork(req.body)
        res.json('ok')
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/deleteNetwork', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        // delete SSID in wpa_supplicant.conf file
        const wifi = await Wifi.getInstance()
        wifi.removeNetworkFromWpaFile(req.body.SSID)

        res.json('ok')
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/install', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        //restart sensor container, so config is re-read
        res.json(shell.exec('docker restart sensor'))
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/reboot', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        res.json('ok');
        shell.exec('reboot', { silent: true });
    }
    else {
        res.redirect(401, '/login');
    }
})

router.post('/poweroff', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        res.json('ok');
        shell.exec('poweroff', { silent: true });
    }
    else {
        res.redirect(401, '/login');
    }
})

// router.post('/reset', async (req, res) => {
//     sess = req.session;
//     if (sess.username) {
//         res.json('ok');
//         const gpio = await Gpio.getInstance()
//         gpio.resetModule();
//     }
//     else {
//         res.redirect(401, '/login');
//     }
// })

router.post('/login', async (req, res) => {
    sess = req.session;
    const config = await Config.getInstance()
    const hash = config.getPasswordHash();
    if (req.body.username == config.username) {
        bcrypt.compare(req.body.password, hash)
            .then(function (correct) {
                if (correct) {
                    sess.username = req.body.username;
                    res.json({ message: 'ok' });
                } else {
                    // error msg
                    res.json({ message: 'username or password is wrong' });
                }
            });
    } else {
        // error msg
        res.json({ message: 'username or password is wrong' });
    }
})

router.post('/change-pswd', async (req, res) => {
    sess = req.session;
    const config = await Config.getInstance()
    const hash = config.getPasswordHash();

    if (sess.username) {
        if (req.body.username == config.username) {
            bcrypt.compare(req.body.oldpassword, hash)
                .then(function (correct) {
                    if (correct) {
                        if (req.body.newpassword == req.body.rptpassword) {
                            var newpswd = bcrypt.hashSync(req.body.newpassword, 10)
                            // set config pswd hash to new pswd hash
                            config.passwordHash = newpswd;
                            config.save();
                            res.json({ message: 'ok' })
                            Log.log(Log.types.INFO, "Password has been changed")
                        }
                        else {
                            res.json({ message: 'New passwords are not the same' });
                        }

                    } else {
                        // error msg
                        res.json({ message: 'username or old password is wrong' });
                    }
                })
                .catch(error => Log.log(Log.types.ERROR, `Failed to check old password: ${error.message}`));
        } else {
            // error msg
            res.json({ message: 'username or password is wrong' });
        }
    } else {
        res.redirect(401, '/login');
    }
})

router.post('/GetLogs', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        // lees log files
        var logFiles = ["/var/log/climate-monitor/installer.log", "/var/log/climate-monitor/sensor.log"];
        var sepperator = ";;\r\n\r\n"

        // show when there were updates
        var installerWatch = "/root/.pm2/pm2.log"
        var packageJsonWatch = "/home/pi/.pm2/logs/watch-installer-package.json-out.log"
        var sensorContainerWatch = "";
        config.files.forEach(f => {
            if (f.name == "docker-ouroboros.log")
                sensorContainerWatch = f.path;
        })

        // updates docker container => ouroboros log file => search for "'name' will be updated\n"
        var result = [];
        logFiles.forEach((path) => {
            try {
                var data = fs.readFileSync(path, 'utf-8');
                array = data.split(sepperator);
                result = result.concat(array);
            } catch (error) {
                Log.log(Log.types.ERROR, `failed to read log file: ${error.message}`);
            }
        });

        // search for changes in backend js files
        if (fs.existsSync(installerWatch)) {
            var data = fs.readFileSync(installerWatch, 'utf-8')
            var fileAsArray = data.split("\n");
            fileAsArray.forEach(line => {
                if (line.includes('Change detected on path')) {
                    var time = line.split(': ')[0]
                    var file = line.split(': ')[2].split(' ')[4]
                    var text = `[ ${new Date(time).toUTCString()} ] [ info ] [ pm2 ] File ${file} changed: installer code updated`;
                    result.push(text)
                }
            })
        } else {
            Log.log(Log.types.ERROR, `Failed to read ${installerWatch}`)
        }

        // search for changes in package.json file
        if (fs.existsSync(packageJsonWatch)) {
            var data = fs.readFileSync(packageJsonWatch, 'utf-8')
            var fileAsArray = data.split("\n");
            for (var i = 0; i < fileAsArray.length; i++) {
                if (fileAsArray[i].includes('change:/home/pi/installer/package.json')) {
                    var date = fileAsArray[i + 1];
                    var message = fileAsArray[i + 2];
                    var text = `[ ${new Date(date).toUTCString()} ] [ info ] [ pm2 ] installer/package.json changed: ${message}`;
                    result.push(text)
                }
            }
        } else {
            Log.log(Log.types.ERROR, `Failed to read ${packageJsonWatch}`)
        }

        // search for sensor container updates in log file 
        if (fs.existsSync(sensorContainerWatch)) {
            var data = fs.readFileSync(sensorContainerWatch, 'utf-8')
            var fileAsArray = data.split("\n");
            fileAsArray.forEach(line => {
                if (line.includes('sensor will be updated')) {
                    var asJson = JSON.parse(line);
                    var date = asJson.time;
                    var text = `[ ${new Date(date).toUTCString()} ] [ info ] [ docker ] docker sensor container update`;
                    result.push(text)
                }
            })
        } else {
            Log.log(Log.types.ERROR, `Failed to read ${sensorContainerWatch}`)
        }

        res.send(result)
    }
    else {
        res.json({ message: 'login' })
    }
});

router.post('/downloadFile', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        var downloadFiles = config.files;
        for (var i = 0; i < downloadFiles.length; i++) {
            if (downloadFiles[i].path == req.body.file) {
                return res.download(req.body.file);
            }
        }
        res.json({ message: 'file not found' })
    }
    else {
        res.json({ message: 'login' })
    }
})

router.post('/removeFile', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        var downloadFiles = config.files;
        for (var i = 0; i < downloadFiles.length; i++) {
            if (downloadFiles[i].path == req.body.file) {
                fs.unlinkSync(req.body.file);
                return res.json({ message: 'ok' })
            }
        }
        res.json({ message: 'file not found' })
    }
    else {
        res.json({ message: 'login' })
    }
    config.getDownloadFiles();
})

router.post('/controlLeds', async (req, res) => {
    sess = req.session;
    if (sess.username) {
        const config = await Config.getInstance()
        config.update(req.body)
        res.json({ message: 'ok' })
    }
    else {
        res.redirect(401, '/login');
    }
})



module.exports = router
