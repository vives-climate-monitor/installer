exports = module.exports = function (IOserver, IOclient) {
    
    IOserver.sockets.on('connection', socket => {
        // update networks of wpa_supplicant file
        socket.on('updateNetwork', msg => {
            Log.log('msg')
        })

        // update broadcasted networks
        socket.on('updateBroadcastedNetworks', () => {
            Log.log('updateBroadcastedNetworks triggered')
        })
    })

    IOclient.on('co2 messure',msg => IOserver.emit('co2 messure', msg));
    IOclient.on('tvoc messure',msg => IOserver.emit('tvoc messure', msg));
    IOclient.on('temp messure',msg => IOserver.emit('temp messure', msg));
    IOclient.on('hum messure',msg => IOserver.emit('hum messure', msg));
    IOclient.on('time messure',msg => IOserver.emit('time messure', msg));


}
