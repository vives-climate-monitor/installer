module.exports = {
  apps: [{
    script: 'app.js',
    name: 'installer',
    watch: [
      "./",                   // watch everything, except the files/folders that are ignored => if you add new file or folder in root directory, will this be watched automatically.
    ],
    watch_delay: 1000,
    ignore_watch: [
      "**/node_modules/**",
      "**/public/**",
      "**/components/**",
      "**/ansible/**",
      "**/views/**",
      "**/data/**",
      ".gitignore",
      "manifest.json",
      "serviceworker.js",
      "package.json",
      "package-lock.js",
      "docker-compose.yml",
      "**/.git/**",
      ],
    watch_options: {
      followSymlinks: false,
      persistent: true,
    }
  }]
};


// when changing this file, you need to do:
//    sudo pm2 stop installer
//    sudo pm2 del installer
//    sudo pm2 start ecosystem.config.js
// otherwise, changes will not be updated
