const express = require('express')
const path = require('path')

const app = express()
const http = require('http').createServer(app)
const IOserver = require('socket.io')(http)
const IOclient = require('socket.io-client')('http://localhost:3000')
const socketFile = require('./sockets/index')(IOserver, IOclient)
var session = require('express-session')
require('dotenv').config()

const Config = require('./classes/config');
const Wifi = require('./classes/wifi');
const Reset = require('./classes/reset.js');
const Log = require('./classes/consoleLog');


const PORT = process.env.PORT || 80

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))
app.use(session({secret: 'vxburvdo',saveUninitialized: true,resave: true}));

// routers
const indexRouter = require('./routes/index')
const Gpio = require('./classes/reset.js')

app.use('/', indexRouter)

// start server
http.listen(PORT, async () => {
    const config = await Config.getInstance();
    const wifi = await Wifi.getInstance();
    const reset = await Reset.getInstance();
    Log.log(Log.types.INFO, `Local webserver running on port ${PORT}`);
    config.scanHostname();
})